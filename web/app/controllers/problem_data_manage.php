<?php
	requirePHPLib('form');
	requirePHPLib('judger');
	requirePHPLib('data');
	
	if (!validateUInt($_GET['id']) || !($problem = queryProblemBrief($_GET['id']))) {
		become404Page();
	}
	if (!hasProblemPermission($myUser, $problem)) {
		become403Page();
	}
	
	$oj_name = UOJConfig::$data['profile']['oj-name'];
	$problem_extra_config = getProblemExtraConfig($problem);

	//$data_dir = "/var/uoj_data/${problem['id']}";
	$data_dir = "/var/uoj_data/upload/${problem['id']}";
	function readConfig($filename)
	{
		$f = fopen($filename, "r");
		$config = [];
		while (($buffer = fgets($f, 4096)) !== false) {
			$buffer = str_replace(PHP_EOL, '', $buffer); #去除读出的每一行中的回车
			if ($buffer == "") {
				continue;
			}
			$config_line = explode(" ", $buffer);      #使用空格隔开已有的配置变量名与变量值
			$config[$config_line[0]] = $config_line[1];  #放入config数组中
			echo $config_line[0],$config_line[1];
		}
		fclose($f);
		return $config;
	}

	function echoFileNotFound($file_name) {
		echo '<h4>', htmlspecialchars($file_name), '<sub class="text-danger"> ', '文件未找到', '</sub></h4>';
	}
	function echoFilePre($file_name) {
		global $data_dir;
		$file_full_name = $data_dir . '/' . $file_name;

		$finfo = finfo_open(FILEINFO_MIME);
		$mimetype = finfo_file($finfo, $file_full_name);
		if ($mimetype === false) {
			echoFileNotFound($file_name);
			return;
		}
		finfo_close($finfo);

		echo '<h4>', htmlspecialchars($file_name), '<sub> ', $mimetype, '</sub></h4>';
		echo "<pre>\n";

		$output_limit = 1000;
		if (strStartWith($mimetype, 'text/')) {
			echo htmlspecialchars(uojFilePreview($file_full_name, $output_limit));
		} else {
			echo htmlspecialchars(uojFilePreview($file_full_name, $output_limit, 'binary'));
		}
		echo "\n</pre>";
	}
	function deldir($dir) {
		//先删除目录下的文件：
		$dh=opendir($dir);
		while ($file=readdir($dh)) {
		  if($file!="." && $file!="..") {
			$fullpath=$dir."/".$file;
			if(!is_dir($fullpath)) {
				unlink($fullpath);
			} else {
				deldir($fullpath);
			}
		  }
		}
		closedir($dh);
	  }

	if ($_POST['problem_datapoint_file_submit'] == 'submit') {
		if ($_FILES["problem_datapoint_file"]["error"] == 0 && isset($_POST["name"])) {
			$datafilename = "/var/uoj_data/upload/{$problem['id']}/" . $_POST["name"];
			$status = move_uploaded_file($_FILES["problem_datapoint_file"]["tmp_name"], $datafilename);
			echo $status;
			die();
		}
	}
	//题型设置 Step-1
	
	if ($_POST['problem_type_submit'] == 'submit') {
		if($_POST['problem_type']) {
			deldir("/var/uoj_data/upload/{$problem['id']}/");
			$type_filename="/var/uoj_data/upload/{$problem['id']}/type.conf"; #用于临时存储问题的类型
			if (file_exists($type_filename)) {
				unlink($type_filename);
			}
			$typefile = fopen($type_filename, "a");
			fwrite($typefile, $_POST['problem_type']);
			fclose($typefile);
			$set_filename="/var/uoj_data/upload/{$problem['id']}/problem.conf";
			if (file_exists($set_filename)) {
				unlink($set_filename);
			} 
			$setfile = fopen($set_filename, "a");
			//每种题型都有一些预设的设置
			switch($_POST['problem_type']) {
				case "classical":
					fwrite($setfile, "use_builtin_judger on\n");
					break;
				case "submit_answer":
					fwrite($setfile, "use_builtin_judger on\n");
					fwrite($setfile, "submit_answer on\n");
					break;
				case "interactive":
					fwrite($setfile, "use_builtin_judger on\n");
					fwrite($setfile, "with_implementer on\n");
					fwrite($setfile, "answer_unit_name data\n");

			}
			fclose($setfile);
		}
	}
	//基础信息设置 Step-2
	if ($_POST['problem_basic_info_submit'] == 'submit') {
		if ($_POST['time_limit'] and $_POST['memory_limit']) {
			$set_filename="/var/uoj_data/upload/{$problem['id']}/problem.conf"; 
			$setfile = fopen($set_filename, "a");
			fwrite($setfile, "time_limit ".$_POST['time_limit']."\n");
			fwrite($setfile, "memory_limit ".$_POST['memory_limit']."\n");
			if(isset($_POST["output_limit"])) {
				fwrite($setfile, "output_limit ".$_POST['output_limit']."\n");
			}
			fclose($setfile);
		}
	}
	//数据点设置 Step-3
	if ($_POST['problem_data_point_submit'] == 'submit') {
		if (isset($_POST['n_tests'])) {
			$set_filename="/var/uoj_data/upload/{$problem['id']}/problem.conf";
			$setfile = fopen($set_filename, "a");
			fwrite($setfile, "n_tests ".$_POST['n_tests']."\n");
			if (isset($_POST["ex_tests_check"]) and isset($_POST['n_ex_tests']) and isset($_POST['n_sample_tests'])) {
				fwrite($setfile, "n_ex_tests ". ($_POST['n_ex_tests'] + $_POST['n_sample_tests']) ."\n");
				fwrite($setfile, "n_sample_tests ".$_POST['n_sample_tests']."\n");
			} else if(isset($_POST["n_sample_tests"])){
				fwrite($setfile, "n_ex_tests ".$_POST['n_sample_tests']."\n");
				fwrite($setfile, "n_sample_tests ".$_POST['n_sample_tests']."\n");
			}
			
			if (isset($_POST['use_builtin_checker_check'])) {
				fwrite($setfile, "use_builtin_checker ".$_POST['use_builtin_checker']."\n");
			}
			if (isset($_POST['token'])) {
				fwrite($setfile, "token ".$_POST['token']."\n");
			}
			fclose($setfile);
		}
	}

	//数据点输入设置 Step-4
	if($_POST['problem_data_submit'] == 'submit') {
		$valid = true;
		$set_filename= "/var/uoj_data/upload/{$problem['id']}/problem.conf";
		$config = readConfig($set_filename);
		for ($i = 1; $i <= intval($config['n_tests']); $i++) {
			if(!isset($_POST['check_' . $i . '_in']) || !isset($_POST['check_' . $i . '_out'])) {
				$valid = false;
			}
		}
		for ($i = 1; $i <= intval($config['n_ex_tests']); $i++) {
			if(!isset($_POST['ex_check_' . $i . '_in']) || !isset($_POST['ex_check_' . $i . '_out'])) {
				$valid = false;
			}
		}
		if (!isset($_POST['point_score'])) {
			$valid = false;
		}
		if ($valid) {
			$set_filename="/var/uoj_data/upload/{$problem['id']}/problem.conf"; 
			$setfile = fopen($set_filename, "a");
			fwrite($setfile, "input_pre data\n");
			fwrite($setfile, "input_suf in\n");
			fwrite($setfile, "output_pre data\n");
			fwrite($setfile, "output_suf out\n");
			$point = $_POST['point_score'];
			$point = str_replace(PHP_EOL, '', $point);
			if ($point != "" && !isset($_POST["point_average"])) {
				$points = explode(" ", $point);
				foreach ($points as $i => $point_score) {
					fwrite($setfile, "point_score_". ($i + 1) ." {$point_score}\n");
				}
			}
			
			fclose($setfile);
			if (isset($_POST["chk_code"])) {
				$chk_filename="/var/uoj_data/upload/{$problem['id']}/chk.cpp";
				$chkfile = fopen($chk_filename, "w");
				fwrite($chkfile, $_POST["chk_code"]);
				fclose($chkfile);
			}
			if (isset($_POST["judger_code"])) {
				$val_filename="/var/uoj_data/upload/{$problem['id']}/val.cpp";
				$valfile = fopen($val_filename, "w");
				fwrite($valfile, $_POST["judger_code"]);
				fclose($valfile);
			}
			
			for ($i = 1; $i <= intval($config['n_tests']); $i++) {
				$input_file = "/var/uoj_data/upload/{$problem['id']}/data" . $i . ".in";
				$input = fopen($input_file, "w");
				fwrite($input, $_POST['check_' . $i . '_in']);
				fclose($input);
			}
			for ($i = 1; $i <= intval($config['n_tests']); $i++) {
				$output_file = "/var/uoj_data/upload/{$problem['id']}/data" . $i . ".out";
				$output = fopen($output_file, "w");
				fwrite($output, $_POST['check_' . $i . '_out']);
				fclose($output);
			}
			for ($i = 1; $i <= intval($config['n_ex_tests']); $i++) {
				$input_file = "/var/uoj_data/upload/{$problem['id']}/ex_data" . $i . ".in";
				$input = fopen($input_file, "w");
				fwrite($input, $_POST['ex_check_' . $i . '_in']);
				fclose($input);
			}
			for ($i = 1; $i <= intval($config['n_ex_tests']); $i++) {
				$output_file = "/var/uoj_data/upload/{$problem['id']}/ex_data" . $i . ".out";
				$output = fopen($output_file, "w");
				fwrite($output, $_POST['ex_check_' . $i . '_out']);
				fclose($output);
			}
			
		}
	}


	//上传数据
	if ($_POST['problem_data_file_submit']=='submit') {
		if ($_FILES["problem_data_file"]["error"] > 0) {
			$errmsg = "Error: ".$_FILES["problem_data_file"]["error"];
			becomeMsgPage('<div>' . $errmsg . '</div><a href="/problem/'.$problem['id'].'/manage/data">返回</a>');
		} else {
			$zip_mime_types = array('application/zip', 'application/x-zip', 'application/x-zip-compressed');
			if (in_array($_FILES["problem_data_file"]["type"], $zip_mime_types)) {
				$up_filename="/tmp/".rand(0,100000000)."data.zip";
				move_uploaded_file($_FILES["problem_data_file"]["tmp_name"], $up_filename);
				$zip = new ZipArchive;
				if ($zip->open($up_filename) === TRUE) {
					$zip->extractTo("/var/uoj_data/upload/{$problem['id']}");
					$zip->close();
					exec("cd /var/uoj_data/upload/{$problem['id']}; if [ `find . -maxdepth 1 -type f`File = File ]; then for sub_dir in `find -maxdepth 1 -type d ! -name .`; do mv -f \$sub_dir/* . && rm -rf \$sub_dir; done; fi");
					echo "<script>alert('上传成功！')</script>";
				} else {
					$errmsg = "解压失败！";
					becomeMsgPage('<div>' . $errmsg . '</div><a href="/problem/'.$problem['id'].'/manage/data">返回</a>');
				}
				unlink($up_filename);
			} else {
				$errmsg = "请上传zip格式！";
				becomeMsgPage('<div>' . $errmsg . '</div><a href="/problem/'.$problem['id'].'/manage/data">返回</a>');
			}
		}
	}

	//添加配置文件
	if ($_POST['problem_settings_file_submit']=='submit') {
		if ($_POST['use_builtin_checker'] and $_POST['n_tests'] and $_POST['input_pre'] and $_POST['input_suf'] and $_POST['output_pre'] and $_POST['output_suf'] and $_POST['time_limit'] and $_POST['memory_limit']) {
			$set_filename="/var/uoj_data/upload/{$problem['id']}/problem.conf";
			$has_legacy=false;
			if (file_exists($set_filename)) {
				$has_legacy=true;
				unlink($set_filename);
			}
			$setfile = fopen($set_filename, "w");
			fwrite($setfile, "use_builtin_judger on\n");
			if ($_POST['use_builtin_checker'] != 'ownchk') {
				fwrite($setfile, "use_builtin_checker ".$_POST['use_builtin_checker']."\n");
			}
			fwrite($setfile, "n_tests ".$_POST['n_tests']."\n");
			if ($_POST['n_ex_tests']) {
				fwrite($setfile, "n_ex_tests ".$_POST['n_ex_tests']."\n");
			} else {
				fwrite($setfile, "n_ex_tests 0\n");
			}
			if ($_POST['n_sample_tests']) {
				fwrite($setfile, "n_sample_tests ".$_POST['n_sample_tests']."\n");
			} else {
				fwrite($setfile, "n_sample_tests 0\n");
			}
			fwrite($setfile, "input_pre ".$_POST['input_pre']."\n");
			fwrite($setfile, "input_suf ".$_POST['input_suf']."\n");
			fwrite($setfile, "output_pre ".$_POST['output_pre']."\n");
			fwrite($setfile, "output_suf ".$_POST['output_suf']."\n");
			fwrite($setfile, "time_limit ".$_POST['time_limit']."\n");
			fwrite($setfile, "memory_limit ".$_POST['memory_limit']."\n");
			fclose($setfile);
			if (!$has_legacy) {
				echo "<script>alert('添加成功！')</script>";
			} else {
				echo "<script>alert('替换成功!')</script>";
			}
		} else {
			$errmsg = "添加配置文件失败，请检查是否所有输入框都已填写！";
			becomeMsgPage('<div>' . $errmsg . '</div><a href="/problem/'.$problem['id'].'/manage/data">返回</a>');
		}
	}


	$info_form = new UOJForm('info');
	$http_host = HTML::escape(UOJContext::httpHost());
	$download_url = HTML::url("/download.php?type=problem&id={$problem['id']}");
	$info_form->appendHTML(<<<EOD
<div class="form-group row">
	<!--<label class="col-sm-3 control-label">zip上传数据</label>
	<div class="col-sm-9">
		<div class="form-control-static">
			<row>
			<button type="button" style="width:30%" class="btn btn-primary" data-toggle="modal" data-target="#UploadDataModal">上传数据</button>
			<button type="submit" style="width:30%" id="button-submit-data" name="submit-data" value="data" class="btn btn-danger">检验配置并同步数据</button>
			</row>
		</div>
	</div>-->
</div>
EOD
	);
	$info_form->appendHTML(<<<EOD
<div class="form-group row">
	<label class="col-sm-3 control-label">problem_{$problem['id']}.zip</label>
	<div class="col-sm-9">
		<div class="form-control-static">
			<a href="$download_url">$download_url</a>
		</div>
	</div>
</div>
EOD
	);
	$info_form->appendHTML(<<<EOD
<div class="form-group row">
	<label class="col-sm-3 control-label">testlib.h</label>
	<div class="col-sm-9">
		<div class="form-control-static">
			<a href="/download.php?type=testlib.h">下载</a>
		</div>
	</div>
</div>
EOD
	);

	$esc_submission_requirement = HTML::escape(json_encode(json_decode($problem['submission_requirement']), JSON_PRETTY_PRINT));
	$info_form->appendHTML(<<<EOD
<div class="form-group row">
	<label class="col-sm-3 control-label">提交文件配置</label>
	<div class="col-sm-9">
		<div class="form-control-static"><pre>
$esc_submission_requirement
</pre>
		</div>
	</div>
</div>
EOD
	);
	$esc_extra_config = HTML::escape(json_encode(json_decode($problem['extra_config']), JSON_PRETTY_PRINT));
	$info_form->appendHTML(<<<EOD
<div class="form-group row">
	<label class="col-sm-3 control-label">其它配置</label>
	<div class="col-sm-9">
		<div class="form-control-static"><pre>
$esc_extra_config
</pre>
		</div>
	</div>
</div>
EOD
	);
	if (isSuperUser($myUser)) {
		$info_form->addVInput('submission_requirement', 'text', '提交文件配置', $problem['submission_requirement'],
			function ($submission_requirement, &$vdata) {
				$submission_requirement = json_decode($submission_requirement, true);
				if ($submission_requirement === null) {
					return '不是合法的JSON';
				}
				$vdata['submission_requirement'] = json_encode($submission_requirement);
			},
			null);
		$info_form->addVInput('extra_config', 'text', '其它配置', $problem['extra_config'],
			function ($extra_config, &$vdata) {
				$extra_config = json_decode($extra_config, true);
				if ($extra_config === null) {
					return '不是合法的JSON';
				}
				$vdata['extra_config'] = json_encode($extra_config);
			},
			null);
		$info_form->handle = function(&$vdata) {
			global $problem;
			$esc_submission_requirement = DB::escape($vdata['submission_requirement']);
			$esc_extra_config = DB::escape($vdata['extra_config']);
			DB::update("update problems set submission_requirement = '$esc_submission_requirement', extra_config = '$esc_extra_config' where id = {$problem['id']}");
		};
	} else {
		$info_form->no_submit = true;
	}

	class DataDisplayer {
		public $problem_conf = array();
		public $data_files = array();
		public $displayers = array();

		public function __construct($problem_conf = null, $data_files = null) {
			global $data_dir;

			if (isset($problem_conf)) {
				foreach ($problem_conf as $key => $val) {
					$this->problem_conf[$key] = array('val' => $val);
				}
			}

			if (!isset($data_files)) {
				$this->data_files = array_filter(scandir($data_dir), function($x) {
					return $x !== '.' && $x !== '..' && $x !== 'problem.conf';
				});
				natsort($this->data_files);
				array_unshift($this->data_files, 'problem.conf');
			} else {
				$this->data_files = $data_files;
			}

			$this->setDisplayer('problem.conf', function($self) {
				global $info_form;
				$info_form->printHTML();
				echo '<div class="top-buffer-md"></div>';

				echo '<table class="table table-bordered table-hover table-striped table-text-center">';
				echo '<thead>';
				echo '<tr>';
				echo '<th>key</th>';
				echo '<th>value</th>';
				echo '</tr>';
				echo '</thead>';
				echo '<tbody>';
				foreach ($self->problem_conf as $key => $info) {
					if (!isset($info['status'])) {
						echo '<tr>';
						echo '<td>', htmlspecialchars($key), '</td>';
						echo '<td>', htmlspecialchars($info['val']), '</td>';
						echo '</tr>';
					} elseif ($info['status'] == 'danger') {
						echo '<tr class="text-danger">';
						echo '<td>', htmlspecialchars($key), '</td>';
						echo '<td>', htmlspecialchars($info['val']), ' <span class="glyphicon glyphicon-remove"></span>', '</td>';
						echo '</tr>';
					}
				}
				echo '</tbody>';
				echo '</table>';

				echoFilePre('problem.conf');
			});
		}

		public function setProblemConfRowStatus($key, $status) {
			$this->problem_conf[$key]['status'] = $status;
			return $this;
		}

		public function setDisplayer($file_name, $fun) {
			$this->displayers[$file_name] = $fun;
			return $this;
		}
		public function addDisplayer($file_name, $fun) {
			$this->data_files[] = $file_name;
			$this->displayers[$file_name] = $fun;
			return $this;
		}
		public function echoDataFilesList($active_file) {
			foreach ($this->data_files as $file_name) {
				echo '<li class="nav-item">';
				if ($file_name != $active_file) {
					echo '<a class="nav-link" href="#">';
				} else {
					echo '<a class="nav-link active" href="#">';
				}
				echo htmlspecialchars($file_name), '</a>', '</li>';
			}
		}
		public function displayFile($file_name) {
			global $data_dir;

			if (isset($this->displayers[$file_name])) {
				$fun = $this->displayers[$file_name];
				$fun($this);
			} elseif (in_array($file_name, $this->data_files)) {
				echoFilePre($file_name);
			} else {
				echoFileNotFound($file_name);
			}
		}
	}

	function getDataDisplayer() {
		global $data_dir;
		global $problem;

		$allow_files = array_flip(array_filter(scandir($data_dir), function($x) {
			return $x !== '.' && $x !== '..';
		}));

		$getDisplaySrcFunc = function($name) use ($allow_files) {
			return function() use ($name, $allow_files) {
				$src_name = $name . '.cpp';
				if (isset($allow_files[$src_name])) {
					echoFilePre($src_name);
				} else {
					echoFileNotFound($src_name);
				}
				if (isset($allow_files[$name])) {
					echoFilePre($name);
				} else {
					echoFileNotFound($name);
				}
			};
		};

		$problem_conf = getUOJConf("$data_dir/problem.conf");
		if ($problem_conf === -1) {
			return (new DataDisplayer())->setDisplayer('problem.conf', function() {
				global $info_form;
				$info_form->printHTML();
				echoFileNotFound('problem.conf');
			});
		}
		if ($problem_conf === -2) {
			return (new DataDisplayer())->setDisplayer('problem.conf', function() {
				global $info_form;
				$info_form->printHTML();
				echo '<h4 class="text-danger">problem.conf 格式有误</h4>';
				echoFilePre('problem.conf');
			});
		}

		$judger_name = getUOJConfVal($problem_conf, 'use_builtin_judger', null);
		if (!isset($problem_conf['use_builtin_judger'])) {
			return new DataDisplayer($problem_conf);
		}
		if ($problem_conf['use_builtin_judger'] == 'on') {
			$n_tests = getUOJConfVal($problem_conf, 'n_tests', 10);
			if (!validateUInt($n_tests)) {
				return (new DataDisplayer($problem_conf))->setProblemConfRowStatus('n_tests', 'danger');
			}

			$has_extra_tests = !(isset($problem_conf['submit_answer']) && $problem_conf['submit_answer'] == 'on');

			$data_disp = new DataDisplayer($problem_conf, array('problem.conf'));
			$data_disp->addDisplayer('tests',
				function($self) use ($problem_conf, $allow_files, $n_tests, $n_ex_tests) {
					for ($num = 1; $num <= $n_tests; $num++) {
						$input_file_name = getUOJProblemInputFileName($problem_conf, $num);
						$output_file_name = getUOJProblemOutputFileName($problem_conf, $num);
						echo '<div class="row">';
						echo '<div class="col-md-6">';
						if (isset($allow_files[$input_file_name])) {
							echoFilePre($input_file_name);
						} else {
							echoFileNotFound($input_file_name);
						}
						echo '</div>';
						echo '<div class="col-md-6">';
						if (isset($allow_files[$output_file_name])) {
							echoFilePre($output_file_name);
						} else {
							echoFileNotFound($output_file_name);
						}
						echo '</div>';
						echo '</div>';
					}
				}
			);
			if ($has_extra_tests) {
				$n_ex_tests = getUOJConfVal($problem_conf, 'n_ex_tests', 0);
				if (!validateUInt($n_ex_tests)) {
					return (new DataDisplayer($problem_conf))->setProblemConfRowStatus('n_ex_tests', 'danger');
				}

				$data_disp->addDisplayer('extra tests',
					function($self) use ($problem_conf, $allow_files, $n_tests, $n_ex_tests) {
						for ($num = 1; $num <= $n_ex_tests; $num++) {
							$input_file_name = getUOJProblemExtraInputFileName($problem_conf, $num);
							$output_file_name = getUOJProblemExtraOutputFileName($problem_conf, $num);
							echo '<div class="row">';
							echo '<div class="col-md-6">';
							if (isset($allow_files[$input_file_name])) {
								echoFilePre($input_file_name);
							} else {
								echoFileNotFound($input_file_name);
							}
							echo '</div>';
							echo '<div class="col-md-6">';
							if (isset($allow_files[$output_file_name])) {
								echoFilePre($output_file_name);
							} else {
								echoFileNotFound($output_file_name);
							}
							echo '</div>';
							echo '</div>';
						}
					}
				);
			}
			
			if (!isset($problem_conf['interaction_mode'])) {
				if (isset($problem_conf['use_builtin_checker'])) {
					$data_disp->addDisplayer('checker', function($self) {
						echo '<h4>use builtin checker : ', $self->problem_conf['use_builtin_checker']['val'], '</h4>';
					});
				} else {
					$data_disp->addDisplayer('checker', $getDisplaySrcFunc('chk'));
				}
			}
			if ($problem['hackable']) {
				$data_disp->addDisplayer('standard', $getDisplaySrcFunc('std'));
				$data_disp->addDisplayer('validator', $getDisplaySrcFunc('val'));
			}
			if (isset($problem_conf['interaction_mode'])) {
				$data_disp->addDisplayer('interactor', $getDisplaySrcFunc('interactor'));
			}
			return $data_disp;
		} else {
			return (new DataDisplayer($problem_conf))->setProblemConfRowStatus('use_builtin_judger', 'danger');
		}
	}

	$data_disp = getDataDisplayer();

	if (isset($_GET['display_file'])) {
		if (!isset($_GET['file_name'])) {
			echoFileNotFound('');
		} else {
			$data_disp->displayFile($_GET['file_name']);
		}
		die();
	}

	$hackable_form = new UOJForm('hackable');
	$hackable_form->handle = function() {
		global $problem;
		$problem['hackable'] = !$problem['hackable'];
		//$problem['hackable'] = 0;
		$ret = dataSyncProblemData($problem);
		if ($ret) {
			becomeMsgPage('<div>' . $ret . '</div><a href="/problem/'.$problem['id'].'/manage/data">返回</a>');
		}
		
		$hackable = $problem['hackable'] ? 1 : 0;
		DB::query("update problems set hackable = $hackable where id = ${problem['id']}");
	};
	$hackable_form->submit_button_config['class_str'] = 'btn btn-warning btn-block';
	$hackable_form->submit_button_config['text'] = $problem['hackable'] ? '禁止使用hack' : '允许使用hack';
	$hackable_form->submit_button_config['smart_confirm'] = '';

	$data_form = new UOJForm('data');
	$data_form->handle = function() {
		global $problem, $myUser;
		set_time_limit(60 * 5);
		$ret = dataSyncProblemData($problem, $myUser);
		if ($ret) {
			becomeMsgPage('<div>' . $ret . '</div><a href="/problem/'.$problem['id'].'/manage/data">返回</a>');
		}
	};
	$data_form->submit_button_config['class_str'] = 'btn btn-danger btn-block';
	$data_form->submit_button_config['text'] = '检验配置并同步数据';
	$data_form->submit_button_config['smart_confirm'] = '';
	
	$clear_data_form = new UOJForm('clear_data');
	$clear_data_form->handle = function() {
		global $problem;
		dataClearProblemData($problem);
	};
	$clear_data_form->submit_button_config['class_str'] = 'btn btn-danger btn-block';
	$clear_data_form->submit_button_config['text'] = '清空题目数据';
	$clear_data_form->submit_button_config['smart_confirm'] = '';

	$rejudge_form = new UOJForm('rejudge');
	$rejudge_form->handle = function() {
		global $problem;
		rejudgeProblem($problem);
	};
	$rejudge_form->succ_href = "/submissions?problem_id={$problem['id']}";
	$rejudge_form->submit_button_config['class_str'] = 'btn btn-danger btn-block';
	$rejudge_form->submit_button_config['text'] = '重测该题';
	$rejudge_form->submit_button_config['smart_confirm'] = '';
	
	$rejudgege97_form = new UOJForm('rejudgege97');
	$rejudgege97_form->handle = function() {
		global $problem;
		rejudgeProblemGe97($problem);
	};
	$rejudgege97_form->succ_href = "/submissions?problem_id={$problem['id']}";
	$rejudgege97_form->submit_button_config['class_str'] = 'btn btn-danger btn-block';
	$rejudgege97_form->submit_button_config['text'] = '重测 >=97 的程序';
	$rejudgege97_form->submit_button_config['smart_confirm'] = '';
	
	$view_type_form = new UOJForm('view_type');
	$view_type_form->addVSelect('view_content_type',
		array('NONE' => '禁止',
				'SELF' => '仅自己',
				'ALL_AFTER_AC' => 'AC后',
				'ALL' => '所有人'
		),
		'查看提交文件:',
		$problem_extra_config['view_content_type']
	);
	$view_type_form->addVSelect('view_all_details_type',
		array('NONE' => '禁止',
				'SELF' => '仅自己',
				'ALL_AFTER_AC' => 'AC后',
				'ALL' => '所有人'
		),
		'查看全部详细信息:',
		$problem_extra_config['view_all_details_type']
	);
	$view_type_form->addVSelect('view_details_type',
		array('NONE' => '禁止',
				'SELF' => '仅自己',
				'ALL_AFTER_AC' => 'AC后',
				'ALL' => '所有人'
		),
		'查看测试点详细信息:',
		$problem_extra_config['view_details_type']
	);
	$view_type_form->handle = function() {
		global $problem, $problem_extra_config;
		$config = $problem_extra_config;
		$config['view_content_type'] = $_POST['view_content_type'];
		$config['view_all_details_type'] = $_POST['view_all_details_type'];
		$config['view_details_type'] = $_POST['view_details_type'];
		$esc_config = DB::escape(json_encode($config));
		DB::query("update problems set extra_config = '$esc_config' where id = '{$problem['id']}'");
	};
	$view_type_form->submit_button_config['class_str'] = 'btn btn-warning btn-block top-buffer-sm';
	
	if ($problem['hackable']) {
		$test_std_form = new UOJForm('test_std');
		$test_std_form->handle = function() {
			global $myUser, $problem;
			
			$user_std = queryUser('std');
			if (!$user_std) {
				becomeMsgPage('请建立"std"账号。');
			}
			
			$requirement = json_decode($problem['submission_requirement'], true);
			
			$zip_file_name = uojRandAvaiableSubmissionFileName();
			$zip_file = new ZipArchive();
			if ($zip_file->open(UOJContext::storagePath().$zip_file_name, ZipArchive::CREATE) !== true) {
				becomeMsgPage('提交失败');
			}
		
			$content = array();
			$content['file_name'] = $zip_file_name;
			$content['config'] = array();
			foreach ($requirement as $req) {
				if ($req['type'] == "source code") {
					$content['config'][] = array("{$req['name']}_language", "C++");
				}
			}
		
			$tot_size = 0;
			foreach ($requirement as $req) {
				$zip_file->addFile("/var/uoj_data/{$problem['id']}/std.cpp", $req['file_name']);
				$tot_size += $zip_file->statName($req['file_name'])['size'];
			}
		
			$zip_file->close();
		
			$content['config'][] = array('validate_input_before_test', 'on');
			$content['config'][] = array('problem_id', $problem['id']);
			$esc_content = DB::escape(json_encode($content));
			$esc_language = DB::escape('C++');
		 	
			$result = array();
			$result['status'] = "Waiting";
			$result_json = json_encode($result);
			$is_hidden = $problem['is_hidden'] ? 1 : 0;
			
			DB::insert("insert into submissions (problem_id, submit_time, submitter, content, language, tot_size, status, result, is_hidden) values ({$problem['id']}, now(), '{$user_std['username']}', '$esc_content', '$esc_language', $tot_size, '{$result['status']}', '$result_json', $is_hidden)");
		};
		$test_std_form->succ_href = "/submissions?problem_id={$problem['id']}";
		$test_std_form->submit_button_config['class_str'] = 'btn btn-danger btn-block';
		$test_std_form->submit_button_config['text'] = '检验数据正确性';
		$test_std_form->runAtServer();
	}
	
	$hackable_form->runAtServer();
	$view_type_form->runAtServer();
	$data_form->runAtServer();
	$clear_data_form->runAtServer();
	$rejudge_form->runAtServer();
	$rejudgege97_form->runAtServer();
	$info_form->runAtServer();
?>
<?php
	$REQUIRE_LIB['dialog'] = '';
?>
<?php echoUOJPageHeader(HTML::stripTags($problem['title']) . ' - 数据 - 题目管理') ?>
<h1 class="page-header" align="center">#<?=$problem['id']?> : <?=$problem['title']?> 管理</h1>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a class="nav-link" href="/problem/<?= $problem['id'] ?>/manage/statement" role="tab">编辑</a></li>
	<li class="nav-item"><a class="nav-link" href="/problem/<?= $problem['id'] ?>/manage/managers" role="tab">管理者</a></li>
	<li class="nav-item"><a class="nav-link active" href="/problem/<?= $problem['id'] ?>/manage/data" role="tab">数据</a></li>
	<li class="nav-item"><a class="nav-link" href="/problem/<?=$problem['id']?>" role="tab">返回</a></li>
</ul>

<div class="row">
	<div class="col-md-10 top-buffer-sm">
		<div class="row">
			<div class="col-md-3 top-buffer-sm" id="div-file_list">
				<ul class="nav nav-pills flex-column">
					<?php $data_disp->echoDataFilesList('problem.conf'); ?>
				</ul>
			</div>
			<div class="col-md-9 top-buffer-sm" id="div-file_content">
				<?php $data_disp->displayFile('problem.conf'); ?>
			</div>
			<script type="text/javascript">
				curFileName = '';
				$('#div-file_list a').click(function(e) {
					$('#div-file_content').html('<h3>Loading...</h3>');
					$(this).tab('show');

					var fileName = $(this).text();
					curFileName = fileName;
					$.get('/problem/<?= $problem['id'] ?>/manage/data', {
							display_file: '',
							file_name: fileName
						},
						function(data) {
							if (curFileName != fileName) {
								return;
							}
							$('#div-file_content').html(data);
						},
						'html'
					);
					return false;
				});
			</script>
		</div>
	</div>
	<div class="col-md-2 top-buffer-sm">

		<div class="top-buffer-md">
			<button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#wizardModal">
				快速设置
			</button>
		</div>
		<br>
		<div class="top-buffer-md">
			<?php if ($problem['hackable']) : ?>
				<span class="glyphicon glyphicon-ok"></span> hack功能已启用
			<?php else: ?>
				<span class="glyphicon glyphicon-remove"></span> hack功能已禁止
			<?php endif ?>
			<?php $hackable_form->printHTML() ?>
		</div>
		<div class="top-buffer-md">
			<?php if ($problem['hackable']) : ?>
				<?php $test_std_form->printHTML() ?>
			<?php endif ?>
		</div>
		<div class="top-buffer-md">
			<button id="button-display_view_type" type="button" class="btn btn-primary btn-block" onclick="$('#div-view_type').toggle('fast');">提交记录可视权限</button>
			<div class="top-buffer-sm" id="div-view_type" style="display:none; padding-left:5px; padding-right:5px;">
				<?php $view_type_form->printHTML(); ?>
			</div>
		</div>
		<div class="top-buffer-md">
			<?php $data_form->printHTML(); ?>
		</div>
		<div class="top-buffer-md">
			<?php $clear_data_form->printHTML(); ?>
		</div>
		<div class="top-buffer-md">
			<?php $rejudge_form->printHTML(); ?>
		</div>
		<div class="top-buffer-md">
			<?php $rejudgege97_form->printHTML(); ?>
		</div>

		<div class="top-buffer-md">
			<button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#UploadDataModal">上传数据</button>
		</div>
		<div class="top-buffer-md">
			<button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#ProblemSettingsFileModal">试题配置</button>
		</div>
	</div>
		<div class="modal fade" id="wizardModal" tabindex="-1" role="dialog" aria-labelledby="wizard" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="wizard">快速设置：#<?= $problem['id'] ?> : <?= $problem['title'] ?></h4>
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					</div>
					<div id="step-1">
						<div class="modal-body">
							<div class="alert alert-danger" role="alert">
								<strong>注意：</strong>快速部署向导会覆盖之前所有的配置数据，点击"下一步"将会清除所有的配置文件。如有需要请备份。
							</div>
							<form class="form-horizontal" action="" method="post" role="form" id="step-1-form">
								<label for="problem_type" class="col-sm-5 control-label">请选择题目类型</label>
									<select class="form-control" id="problem_type" name="problem_type" onclick="showProblemIntro()">
										<option value="classical" selected>传统题</option>
										<option value="submit_answer">提交答案题</option>
										<option value="interactive">交互题</option>
										<option value="transmission">通信题</option>
									</select>
								<br>
								<div id="problem-intro">
									<!-- 问题介绍 -->
									<div class="alert alert-info" role="alert" id="problem-intro-classical">
    									<h4>传统题</h4>
    										每道题都有n个不同的数据点。选手的程序提交到服务器，服务器对选手的程序进行判断。在满足时间限制和内存限制的情况下，选手的程序能够完全匹配数据点的输入输出，则能够得到该数据点的得分。总得分即为题目成绩。
									</div>
								</div>
								
								<input type="hidden" name="problem_type_submit" value="submit">
								
							</form>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
							<button type="button" class="btn btn-primary" onclick="setProblemType();postForm('step-1-form', 1)">下一步</button>
						</div>
					</div>
					<div class="d-none" id="step-2">
						<div class="modal-body">
							<div class="tab-content" id="v-pills-tabContent">
								<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="basic-info">
									<div style="text-align: center;">
										<strong>基础信息设置</strong>
									</div>
									<br>
									<form class="form-horizontal needs-validation" action="" method="post" role="form" id="step-2-form" novalidate>
										<div class="form-group row">
											<label for="time_limit" class="col-sm-5 control-label">时间限制</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" id="time_limit" name="time_limit" pattern="[1-9]\d*" required>
												<div class="invalid-feedback">
        											请正确输入时间限制（正整数）
      											</div>
												<small class="form-text text-muted">单位：秒（不能为小数）</small>
												
											</div>
											
										</div>
										<div class="form-group row">
											<label for="memory_limit" class="col-sm-5 control-label">内存限制</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" id="memory_limit" name="memory_limit" pattern="[1-9]\d*" required>
												<div class="invalid-feedback">
        											请正确输入空间限制（正整数）
      											</div>
												<small class="form-text text-muted">单位：MB</small>
											</div>
										</div>
										<input type="hidden" name="problem_basic_info_submit" value="submit">
									</form>
								</div>
							</div>
							
						</div>
						<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
								<button type="button" class="btn btn-primary" onclick="postForm('step-2-form', 2)">下一步</button>
						</div>

					</div>
					<div class="d-none" id="step-2-interactive">
						<div class="modal-body">
							<div class="tab-content" id="v-pills-tabContent">
								<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="basic-info">
									<div style="text-align: center;">
										<strong>基础信息设置</strong>
									</div>
									<br>
									<form class="form-horizontal needs-validation" action="" method="post" role="form" id="step-2-form-interactive" novalidate>
										<div class="form-group row">
											<label for="time_limit" class="col-sm-5 control-label">时间限制</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" id="time_limit-interactive" name="time_limit" pattern="[1-9]\d*" required>
												<div class="invalid-feedback">
        											请正确输入时间限制（正整数）
      											</div>
												<small class="form-text text-muted">单位：秒（不能为小数）</small>
											</div>
											
										</div>
										<div class="form-group row">
											<label for="memory_limit" class="col-sm-5 control-label">内存限制</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" id="memory_limit-interactive" name="memory_limit" pattern="[1-9]\d*" required>
												<div class="invalid-feedback">
        											请正确输入空间限制（正整数）
      											</div>
												<small class="form-text text-muted">单位：MB</small>
											</div>
										</div>
										<div class="form-group row">
											<label for="output_limit" class="col-sm-5 control-label">输出限制</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" id="output_limit-interactive" name="output_limit" pattern="[1-9]\d*" required>
												<div class="invalid-feedback">
        											请正确输入文件的输出限制（正整数）
      											</div>
												<small class="form-text text-muted">请输入输出文件的字符数限制</small>
											</div>
										</div>
										<input type="hidden" name="problem_basic_info_submit" value="submit">
									</form>
								</div>
							</div>
							
						</div>
						<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
								<button type="button" class="btn btn-primary" onclick="postForm('step-2-form-interactive', 2)">下一步</button>
						</div>

					</div>
					<div class="d-none" id="step-3-classical">
						<div class="modal-body">
							<div class="tab-content" id="v-pills-tabContent">
									<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="basic-info">
										<div style="text-align: center;">
											<strong>数据点设置</strong>
										</div>
										<br>
										<form class="form-horizontal needs-validation" action="" method="post" role="form" id="step-3-form-classical" novalidate>
											<div class="form-group row">
												<label for="n_tests" class="col-sm-5 control-label">数据点个数</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="n_tests-classical" name="n_tests" placeholder="数据点个数" pattern="[1-9]\d*" required>
													<div class="invalid-feedback">
        												请正确输入数据点个数（正整数）
      												</div>
												</div>
											</div>
											<div class="form-group row">
												<label for="n_sample_tests" class="col-sm-5 control-label">样例测试点个数</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="n_sample_tests-classical" name="n_sample_tests" placeholder="样例测试点个数" pattern="[1-9]\d*" required>
													<div class="invalid-feedback">
        												请正确输入测试样例点个数（正整数）
      												</div>
												</div>
											</div>
											
											<div class="form-group form-check">
												<input class="form-check-input" type="checkbox" value="on" id="use_builtin_checker_check-classical" name="use_builtin_checker_check" checked onclick="toggleChecker('use_builtin_checker_check-classical', 'use_builtin_checker_row-classical'); use_builtin_checker = !use_builtin_checker" novalidate>
												<label class="form-check-label" for="use_builtin_checker_check-classical">
													使用内置答案检查器
												</label>
												
											</div>
											<div class="form-group row" id="use_builtin_checker_row-classical">
												<label for="problem_type" class="col-sm-5 control-label">内置答案检查器类型</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="use_builtin_checker-classical" name="use_builtin_checker" placeholder="请输入内置检查器类型">
												</div>
												
											</div>
											<div class="form-group form-check">
												<input class="form-check-input" type="checkbox" value="off" id="ex_tests_check-classical" name="ex_tests_check" onclick="toggleChecker('ex_tests_check-classical', 'ex_tests_row-classical')" novalidate>
												<label class="form-check-label" for="use_builtin_checker_check-classical">
													添加额外数据点
												</label>
												
											</div>
											<div class="form-group row d-none" id="ex_tests_row-classical">
												<label for="n_ex_tests" class="col-sm-5 control-label">
													额外数据点个数&nbsp;
													<div class="glyphicon glyphicon-question-sign" id="extra-help" data-toggle="tooltip" data-placement="top" title="额外测试点是指在 AC 的情况下会测额外数据，如果某个额外数据通不过会被倒扣3分。额外测试点的前几个一定是测试样例，所以你必须把题目中给出的测试样例放到额外测试点当中。"></div>
												</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="n_ex_tests-classical" name="n_ex_tests" placeholder="额外数据点个数" pattern="[1-9]\d*">
													<div class="invalid-feedback">
        												请正确输入额外测试点个数（正整数）
      												</div>
												</div>
											</div>
											<div class="alert alert-info" role="alert">
    											<h4>内置答案检查器</h4>
    											内置答案检查器的类型决定了最后判定答案时所使用的方法。在填写内置检查器类型之前，请阅读<a href="https://universaloj.github.io/post/%E4%BC%A0%E7%BB%9F%E9%A2%98%E9%85%8D%E7%BD%AE.html" target ="_blank">文档</a>中的“校验器”部分，以确保填写正确。
											</div>
											<input type="hidden" name="problem_data_point_submit" value="submit">

										</form>
									</div>
							</div>
							
						</div>
						<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
								<button type="button" class="btn btn-primary" onclick="checkOptionValidity('ex_tests_check-classical', 'n_ex_tests-classical', 'on'); if (postForm('step-3-form-classical', 3)!==false) {updateDataPointNum('step-4-form');}">下一步</button>
						</div>

					</div>
					<div class="d-none" id="step-3-submit_answer">
						<div class="modal-body">
							<div class="tab-content" id="v-pills-tabContent">
									<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="basic-info">
										<div style="text-align: center;">
											<strong>数据点设置</strong>
										</div>
										<br>
										<form class="form-horizontal needs-validation" action="" method="post" role="form" id="step-3-form-submit_answer" novalidation>
											<div class="form-group row">
												<label for="n_tests" class="col-sm-5 control-label">数据点个数</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="n_tests-submit_answer" name="n_tests" placeholder="数据点个数" pattern="[1-9]\d*" required>
													<div class="invalid-feedback">
        												请正确输入数据点个数（正整数）
      												</div>
												</div>
											</div>
											
											<div class="form-group form-check">
												<input class="form-check-input" type="checkbox" value="on" id="use_builtin_checker_check-submit_answer" name="use_builtin_checker_check" checked onclick="toggleChecker('use_builtin_checker_check-submit_answer', 'use_builtin_checker_row-submit_answer'); use_builtin_checker = !use_builtin_checker">
												<label class="form-check-label" for="use_builtin_checker_check-submit_answer">
													使用内置校验器
												</label>
												
											</div>
											<div class="form-group row" id="use_builtin_checker_row-submit_answer">
												<label for="problem_type" class="col-sm-5 control-label">内置答案检查器类型</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="use_builtin_checker-submit_answer" name="use_builtin_checker" placeholder="请输入内置检查器类型">
												</div>
												
											</div>
											
											<div class="alert alert-info" role="alert">
    											<h4>内置答案检查器</h4>
    											内置答案检查器的类型决定了最后判定答案时所使用的方法。在填写内置检查器类型之前，请阅读<a href="https://universaloj.github.io/post/%E4%BC%A0%E7%BB%9F%E9%A2%98%E9%85%8D%E7%BD%AE.html" target ="_blank">文档</a>中的“校验器”部分，以确保填写正确。
											</div>
											<input type="hidden" name="problem_data_point_submit" value="submit">

										</form>
									</div>
							</div>
							
						</div>
						<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
								<button type="button" class="btn btn-primary" onclick="if (postForm('step-3-form-submit_answer', 3) !==false) updateDataPointNum('step-4-form');">下一步</button>
						</div>

					</div>
					<div class="d-none" id="step-3-interactive">
						<div class="modal-body">
							<div class="tab-content" id="v-pills-tabContent">
									<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="basic-info">
										<div style="text-align: center;">
											<strong>数据点设置</strong>
										</div>
										<br>
										<form class="form-horizontal needs-validation" action="" method="post" role="form" id="step-3-form-interactive" novalidate>
											<div class="form-group row">
												<label for="n_tests" class="col-sm-5 control-label">数据点个数</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="n_tests-interactive" name="n_tests" placeholder="数据点个数" pattern="[1-9]\d*" required>
													<div class="invalid-feedback">
        												请正确输入数据点个数（正整数）
      												</div>
												</div>
											</div>
											<div class="form-group row">
												<label for="n_sample_tests" class="col-sm-5 control-label">样例测试点个数</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="n_sample_tests-interactive" name="n_sample_tests" placeholder="样例测试点个数" pattern="[1-9]\d*" required>
													<div class="invalid-feedback">
        												请正确输入测试样例点个数（正整数）
      												</div>
												</div>
											</div>
											
											<div class="form-group form-check">
												<input class="form-check-input" type="checkbox" value="off" id="ex_tests_check-interactive" name="ex_tests_check" onclick="toggleChecker('ex_tests_check-interactive', 'ex_tests_row-interactive')">
												<label class="form-check-label" for="use_builtin_checker_check-interactive">
													添加额外数据点
												</label>
												
											</div>
											<div class="form-group row d-none" id="ex_tests_row-interactive">
												<label for="n_ex_tests" class="col-sm-5 control-label">
													额外数据点个数&nbsp;
													<div class="glyphicon glyphicon-question-sign" id="extra-help" data-toggle="tooltip" data-placement="top" title="额外测试点是指在 AC 的情况下会测额外数据，如果某个额外数据通不过会被倒扣3分。额外测试点的前几个一定是测试样例，所以你必须把题目中给出的测试样例放到额外测试点当中。"></div>
												</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="n_ex_tests-interactive" name="n_ex_tests" placeholder="额外数据点个数">
													<div class="invalid-feedback">
        												请正确输入额外测试点个数（正整数）
      												</div>
												</div>
											</div>
											<div class="form-group row" id="token_row-interactive">
												<label for="n_ex_tests" class="col-sm-5 control-label">
													Token设置
												</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="token-interactive" name="token" placeholder="输入您将在答案检查器代码中使用的Token" required>
													<div class="invalid-feedback">
        												请输入Token
      												</div>
													
												</div>
											</div>
											
											<input type="hidden" name="problem_data_point_submit" value="submit">

										</form>
									</div>
							</div>
							
						</div>
						<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
								<button type="button" class="btn btn-primary" onclick="checkOptionValidity('ex_tests_check-interactive', 'n_ex_tests-interactive', 'on'); if (postForm('step-3-form-interactive', 3)!==false) {updateDataPointNum('step-4-form');}">下一步</button>
						</div>
					</div>
					<div class="d-none" id="step-3-transmission">
						<div class="modal-body">
							<div class="tab-content" id="v-pills-tabContent">
									<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="basic-info">
										<div style="text-align: center;">
											<strong>数据点设置</strong>
										</div>
										<br>
										<form class="form-horizontal needs-validation" action="" method="post" role="form" id="step-3-form-transmission" novalidate>
											<div class="form-group row">
												<label for="n_tests" class="col-sm-5 control-label">数据点个数</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="n_tests-transmission" name="n_tests" placeholder="数据点个数" pattern="[1-9]\d*" required>
													<div class="invalid-feedback">
        												请正确输入数据点个数（正整数）
      												</div>
												</div>
											</div>
											<div class="form-group row">
												<label for="n_sample_tests" class="col-sm-5 control-label">样例测试点个数</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="n_sample_tests-transmission" name="n_sample_tests" placeholder="样例测试点个数" pattern="[1-9]\d*" required>
													<div class="invalid-feedback">
        												请正确输入测试样例点个数（正整数）
      												</div>
												</div>
											</div>
											
											
											
											<input type="hidden" name="problem_data_point_submit" value="submit">

										</form>
									</div>
							</div>
							
						</div>
						<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
								<button type="button" class="btn btn-primary" onclick="if (postForm('step-3-form-transmission', 3)!==false) {updateDataPointNum('step-4-form');}">下一步</button>
						</div>

					</div>
					<div class="d-none" id="step-4">
						<div class="modal-body">
							<div class="tab-content" id="v-pills-tabContent">
									<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="basic-info">
										<div style="text-align: center;">
											<strong>数据点设置</strong>
										</div>
										<br>
										<form class="form-horizontal" action="" method="post" role="form" id="step-4-form">
											
											
												<!-- 这边是数据点 -->
												<!-- <div class="custom-file">
													<input type="file" class="custom-file-input" id="customFile" onchange="uploadData('customFile', 'data1.in')">
													<label class="custom-file-label" for="customFile" id="customFileLabel">选择文件...</label>
													<div class="valid-feedback">
        												上传成功
      												</div>
													<div class="invalid-feedback">
        												上传失败
      												</div>
												</div> -->
											<input type="hidden" name="problem_data_submit" value="submit">

										</form>
									</div>
							</div>
							
						</div>
						<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
								<button type="button" class="btn btn-primary" onclick="postForm('step-4-form', 4)">完成</button>
						</div>

					</div>
					
					
		

					</div>
				</div>
			</div>
		</div>
		<script>
			totalPageNum = 4
			n_tests = 0
			n_ex_tests = 0
			n_sample_tests = 0
			var problemType = ""
			var use_builtin_checker = true
			function uploadData(ele, name, idForDisable) {
				if ($("#" + ele).val() == "") {
					return;
				}
				var formData = new FormData();
				var file = $('#' + ele)[0].files[0];
				var renamedFile = new File([file], name);
				console.log(name);
				console.log(renamedFile);
				formData.append('name', name);
				formData.append('problem_datapoint_file', renamedFile);
				formData.append('problem_datapoint_file_submit', 'submit');
				originText = "";
				$.ajax({
                    url: "",
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false, 
                    beforeSend: function () {
						originText = $("#" + ele + "Label").text();
						$("#" + ele + "Label").text("上传中...");
						
                    },
                    success: function (responseStr) {
                       //11成功后的动作
					   $("#" + ele + "Label").text(file.name);
					   $("#" + ele).addClass("is-valid");
					   $("#" + idForDisable).attr("disabled", "true");
					   $("#" + idForDisable).val("");
					   $("#" + idForDisable).attr("placeholder", "数据已上传，该输入框不再可用");
                    }
                    ,
                    error : function (responseStr) {
                        $("#" + ele + "Label").text(originText);
						$("#" + ele).addClass("is-invalid");
                    }
                });
			}
			function checkOptionValidity(optionId, contentId, status) {
				option = document.getElementById(optionId)
				content = document.getElementById(contentId)
				if (option.value == status) {
					content.setAttribute('required', 'true')
				}else{
					content.removeAttribute('required')
				}
			}
			function setProblemType() {
				var form = document.getElementById('step-1-form');
				var formData = new FormData(form);
    			var object = new Object();//创建一个存放数据的对象
				for (var key of formData.keys()){
					object[key] = formData.get(key);
				}
				problemType = object['problem_type'];
				console.log(problemType);
			}
			
			function toggleChecker(rowName, checkerRow) {
				if ($('#' + rowName).val() == "on") {
					$('#' + rowName).val("off")
					$('#' + checkerRow).addClass("d-none")
					$('#' + checkerRow).attr("disabled", true)
				}else{
					$('#' + rowName).val("on")
					$('#' + checkerRow).removeClass("d-none")
					$('#' + checkerRow).attr("disabled", false)
				}
				
			}

			function toggleCheckerReverse(rowName, checkerRow) {
				if ($('#' + rowName).val() == "on") {
					$('#' + rowName).val("off")
					$('#' + checkerRow).removeClass("d-none")
					$('#' + checkerRow).attr("disabled", false)
				}else{
					$('#' + rowName).val("on")
					$('#' + checkerRow).addClass("d-none")
					$('#' + checkerRow).attr("disabled", true)
				}
				
			}
			function updateDataPointNum(formName) {
				console.log(problemType)
				n_tests = 0
				n_ex_tests = 0
				n_sample_tests = 0
				n_tests = parseInt($("#n_tests-" + problemType).val() === undefined ? 0 : $("#n_tests-" + problemType).val())
				n_ex_tests = parseInt($("#n_ex_tests-" + problemType).val() === undefined ? 0 : $("#n_ex_tests-" + problemType).val())
				n_sample_tests = parseInt($("#n_sample_tests-" + problemType).val() === undefined ? 0 : $("#n_sample_tests-" + problemType).val())
				document.getElementById(formName).innerHTML += '<strong>请输入测试数据点：</strong>'
				for (var i = 1; i<=n_tests; i++) {
					//document.getElementById("step-4-form").innerHTML += '<div class="form-group row"><label for="point_score_' + i + '" class="col-sm-3 control-label">数据点' + i + '分值</label><div class="col-sm-2"><input type="text" class="form-control" id="point_score_' + i + '" name="point_score_' + i + '"></div><div class="col"></div></div>'
					document.getElementById(formName).innerHTML += '<div class="row"><div class="col-sm">\
												<label for="check_' + i + '_in">数据点' + i + '输入</label>\
												<div>\
													<textarea class="form-control" name="check_' + i + '_in" id="check_' + i + '_in" rows="5"  warp="soft" style="overflow-x:scroll;overflow-y:scroll; max-height: 100%; word-wrap: normal;" placeholder="在这里输入，或上传数据文件"></textarea>\
													<br>\
													<div class="custom-file">\
													<input type="file" class="custom-file-input" id="datapoint_' + i + '_in" onchange="uploadData(\'datapoint_' + i + '_in\', \'data' + i + '.in\', \'check_' + i + '_in\')">\
													<label class="custom-file-label" for="datapoint_' + i + '_in" id="datapoint_' + i + '_inLabel">选择数据点' + i + '输入文件...</label>\
													<div class="valid-feedback">\
        												上传成功\
      												</div>\
													  <div class="invalid-feedback">\
        												上传失败\
      												</div>\
													</div>\
												</div>\
											</div>\
											<div class="col-sm">\
												<label for="check_' + i + '_out">数据点' + i + '答案</label>\
												<div>\
													<textarea class="form-control" name="check_' + i + '_out" id="check_' + i + '_out" rows="5" wrap="soft" style="overflow-x:scroll;overflow-y:scroll; max-height: 100%; word-wrap: normal;" placeholder="在这里输入，或上传数据文件"></textarea>\
													<br>\
													<div class="custom-file">\
													<input type="file" class="custom-file-input" id="datapoint_' + i + '_out" onchange="uploadData(\'datapoint_' + i + '_out\', \'data' + i + '.out\', \'check_' + i + '_out\')">\
													<label class="custom-file-label" for="datapoint_' + i + '_out" id="datapoint_' + i + '_outLabel">选择数据点' + i + '答案文件...</label>\
													<div class="valid-feedback">\
        												上传成功\
      												</div>\
													  <div class="invalid-feedback">\
        												上传失败\
      												</div>\
													</div>\
												</div>\
											</div></div><br><br>'
				}
				for (var i = 1; i<=n_sample_tests; i++) {
					//document.getElementById("step-4-form").innerHTML += '<div class="form-group row"><label for="point_score_' + i + '" class="col-sm-3 control-label">数据点' + i + '分值</label><div class="col-sm-2"><input type="text" class="form-control" id="point_score_' + i + '" name="point_score_' + i + '"></div><div class="col"></div></div>'
					document.getElementById(formName).innerHTML += '<div class="row"><div class="col-sm">\
												<label for="ex_check_' + i + '_in">样例数据点' + i + '输入</label>\
												<div>\
													<textarea class="form-control" name="ex_check_' + i + '_in" id="ex_check_' + i + '_in" rows="5"  warp="soft" style="overflow-x:scroll;overflow-y:scroll; max-height: 100%; word-wrap: normal;" placeholder="在这里输入，或上传数据文件"></textarea>\
													<br>\
													<div class="custom-file">\
													<input type="file" class="custom-file-input" id="ex_datapoint_' + i + '_in" onchange="uploadData(\'ex_datapoint_' + i + '_in\', \'ex_data' + i + '.in\', \'ex_check_' + i + '_in\')">\
													<label class="custom-file-label" for="ex_datapoint_' + i + '_in" id="ex_datapoint_' + i + '_inLabel">选择样例数据点' + i + '输入文件...</label>\
													<div class="valid-feedback">\
        												上传成功\
      												</div>\
													  <div class="invalid-feedback">\
        												上传失败\
      												</div>\
													</div>\
												</div>\
											</div>\
											<div class="col-sm">\
												<label for="ex_check_' + i + '_out">样例数据点' + i + '答案</label>\
												<div>\
													<textarea class="form-control" name="ex_check_' + i + '_out" id="ex_check_' + i + '_out" rows="5" wrap="soft" style="overflow-x:scroll;overflow-y:scroll; max-height: 100%; word-wrap: normal;" placeholder="在这里输入，或上传数据文件"></textarea>\
													<br>\
													<div class="custom-file">\
													<input type="file" class="custom-file-input" id="ex_datapoint_' + i + '_out" onchange="uploadData(\'ex_datapoint_' + i + '_out\', \'ex_data' + i + '.out\', \'ex_check_' + i + '_out\')">\
													<label class="custom-file-label" for="ex_datapoint_' + i + '_out" id="ex_datapoint_' + i + '_outLabel">选择样例数据点' + i + '答案文件...</label>\
													<div class="valid-feedback">\
        												上传成功\
      												</div>\
													  <div class="invalid-feedback">\
        												上传失败\
      												</div>\
													</div>\
												</div>\
											</div></div><br><br>'
				}
				for (var i = 1; i<=n_ex_tests; i++) {
					//document.getElementById("step-4-form").innerHTML += '<div class="form-group row"><label for="point_score_' + i + '" class="col-sm-3 control-label">数据点' + i + '分值</label><div class="col-sm-2"><input type="text" class="form-control" id="point_score_' + i + '" name="point_score_' + i + '"></div><div class="col"></div></div>'
					document.getElementById(formName).innerHTML += '<div class="row"><div class="col-sm">\
												<label for="ex_check_' + (i + n_sample_tests) + '_in">额外数据点' + i + '输入</label>\
												<div>\
													<textarea class="form-control" name="ex_check_' + (i + n_sample_tests) + '_in" id="ex_check_' + (i + n_sample_tests) + '_in" rows="5"  warp="soft" style="overflow-x:scroll;overflow-y:scroll; max-height: 100%; word-wrap: normal;" placeholder="在这里输入，或上传数据文件"></textarea>\
													<br>\
													<div class="custom-file">\
													<input type="file" class="custom-file-input" id="ex_datapoint_' + (i + n_sample_tests) + '_in" onchange="uploadData(\'ex_datapoint_' + (i + n_sample_tests) + '_in\', \'ex_data' + (i + n_sample_tests) + '.in\', \'ex_check_' + (i + n_sample_tests) + '_in\')">\
													<label class="custom-file-label" for="ex_datapoint_' + (i + n_sample_tests) + '_in" id="ex_datapoint_' + (i + n_sample_tests) + '_inLabel">选择额外数据点' + i + '输入文件...</label>\
													<div class="valid-feedback">\
        												上传成功\
      												</div>\
													  <div class="invalid-feedback">\
        												上传失败\
      												</div>\
													</div>\
												</div>\
											</div>\
											<div class="col-sm">\
												<label for="ex_check_' + (i + n_sample_tests) + '_out">额外数据点' + i + '答案</label>\
												<div>\
													<textarea class="form-control" name="ex_check_' + (i + n_sample_tests) + '_out" id="ex_check_' + (i + n_sample_tests) + '_out" rows="5" wrap="soft" style="overflow-x:scroll;overflow-y:scroll; max-height: 100%; word-wrap: normal;" placeholder="在这里输入，或上传数据文件"></textarea>\
													<br>\
													<div class="custom-file">\
													<input type="file" class="custom-file-input" id="ex_datapoint_' + (i + n_sample_tests) + '_out" onchange="uploadData(\'ex_datapoint_' + (i + n_sample_tests) + '_out\', \'ex_data' + (i + n_sample_tests) + '.out\', \'ex_check_' + (i + n_sample_tests) + '_out\')">\
													<label class="custom-file-label" for="ex_datapoint_' + (i + n_sample_tests) + '_out" id="ex_datapoint_' + (i + n_sample_tests) + '_outLabel">选择额外数据点' + i + '答案文件...</label>\
													<div class="valid-feedback">\
        												上传成功\
      												</div>\
													  <div class="invalid-feedback">\
        												上传失败\
      												</div>\
													</div>\
												</div>\
											</div></div>'
				}

				
				document.getElementById(formName).innerHTML += '<br><div class="form-group form-check">\
												<input class="form-check-input" type="checkbox" value="on" id="point_average-classical" checked name="point_average" onclick="toggleCheckerReverse(\'point_average-classical\', \'point_score_row-classical\')">\
												<label class="form-check-label" for="point_average">\
													数据点分值平均分配\
												</label>\
											</div>\
											<div class="row d-none" id="point_score_row-classical">\
												<div class="col-sm">\
													<label for="point_score">分值设置</label>\
													<textarea class="form-control" name="point_score" id="point_score" rows="5"  warp="soft" style="overflow-y:scroll; max-height: 100%; word-wrap: normal;" placeholder="请输入各道题的分值，若留空，则分数平均分配。&#13;&#10; 分数用空格隔开，对应相应题目的分数，总和为100。&#13;&#10; 例如：（4个数据点）&#13;&#10; 25 25 25 25"></textarea>\
												</div>\
											</div>'
				if ($("#use_builtin_checker_check-" + problemType).val() == "off" || $("#use_builtin_checker_check-" + problemType).val() === undefined) {
					document.getElementById(formName).innerHTML += '<div class="row" id="chk_code">\
												<div class="col-sm">\
													<label for="chk_code">答案检查器代码</label>\
													<textarea class="form-control" name="chk_code" id="chk_code" rows="5"  warp="soft" style="overflow-y:scroll; max-height: 100%; word-wrap: normal;" placeholder="请输入答案检查器代码（C++）"></textarea>\
												</div>\
											</div>'
				}
				if (problemType == "transmission") {
					document.getElementById(formName).innerHTML += '<div class="row" id="judger_code">\
												<div class="col-sm">\
													<label for="judger_code">数据检验器代码</label>\
													<textarea class="form-control" name="judger_code" id="judger_code" rows="5"  warp="soft" style="overflow-y:scroll; max-height: 100%; word-wrap: normal;" placeholder="请输入数据检验器代码（C++）"></textarea>\
												</div>\
											</div>'
				}
			}
			function showProblemIntro() {
				var value = $("#problem_type").val()
				if(value == "classical") {
					$("#problem-intro").html('<div class="alert alert-info" role="alert" id="problem-intro-classical">\
    											<h4>传统题</h4>\
    												每道题都有n个不同的数据点。选手的程序提交到服务器，服务器对选手的程序进行判断。在满足时间限制和内存限制的情况下，选手的程序能够完全匹配数据点的输入输出，则能够得到该数据点的得分。总得分即为题目成绩。\
											</div>')
				} 
				if (value == "submit_answer") {
				$("#problem-intro").html('<div class="alert alert-info" role="alert">\
    										<h4>提交答案题</h4>\
    										每道题有n个不同的数据点。与传统题不同的是，评测机不会运行选手代码与答案文件校对，而是使用选手自己上传的输出文件与答案文件做对比。因此，该题型不需要设置时空限制。\
										</div>')
				}
				if (value == "interactive") {
				$("#problem-intro").html('<div class="alert alert-info" role="alert">\
    										<h4>交互题</h4>\
    										选手程序与评测机做实时交互，评测机通过对比每次实时交互的输出与答案文件来对选手的程序进行判分。\
										</div>')
				}
				if (value == "transmission") {
				$("#problem-intro").html('<div class="alert alert-info" role="alert">\
    										<h4>通信题</h4>\
    										通信题是需要两个选手程序进行通信，合作完成某项任务的题目。第一个程序接收问题的输入，并产生某些输出；第二个程序的输入会与第一个的输出相关（有时是原封不动地作为一个参数，有时会由评测端处理得到），它需要产生问题的解。\
										</div>')
				}
				
			}
			function isNotNegNum(value) {
				var patrn = /^\d+$/
				if (value != "") {
        			return patrn.test(value)
      			} else {
					  return false
				}
			}
			function nextPage(currentPage) {
				if(currentPage == totalPageNum) {
					console.log("yeah")
					alert("设置成功！")
					$("#wizardModal").modal("hide")
					
				} else {
					switch (problemType) {
						case "classical":
							switch(currentPage) {
								case 1:
									$("#step-1").addClass("d-none");
									$("#step-2").removeClass("d-none");
									break;
								case 2:
									$("#step-2").addClass("d-none");
									$("#step-3-classical").removeClass("d-none");
									break;
								case 3:
									$("#step-3-classical").addClass("d-none");
									$("#step-4").removeClass("d-none");
									break;
							}
							break;
						case "submit_answer":
							switch(currentPage) {
								case 1:
									$("#step-1").addClass("d-none");
									$("#step-3-submit_answer").removeClass("d-none");
									break;
								case 3:
									$("#step-3-submit_answer").addClass("d-none");
									$("#step-4").removeClass("d-none");
									break;
							}
							break;
						case "interactive":
							switch(currentPage) {
								case 1:
									$("#step-1").addClass("d-none");
									$("#step-2-interactive").removeClass("d-none");
									break;
								case 2:
									$("#step-2-interactive").addClass("d-none");
									$("#step-3-interactive").removeClass("d-none");
									break;
								case 3:
									$("#step-3-interactive").addClass("d-none");
									$("#step-4").removeClass("d-none");
									break;
							}
							break;
						case "transmission":
							switch(currentPage) {
								case 1:
									$("#step-1").addClass("d-none");
									$("#step-2").removeClass("d-none");
									break;
								case 2:
									$("#step-2").addClass("d-none");
									$("#step-3-transmission").removeClass("d-none");
									break;
								case 3:
									$("#step-3-transmission").addClass("d-none");
									$("#step-4").removeClass("d-none");
									break;

							}
								
								
					}
					
					
					
				}
			}
			function postForm(formID, currentPage) {
				var form = document.getElementById(formID);//获取要提交的div
				if (form.checkValidity() === false) {
					form.classList.add('was-validated');
					return false;
				}
				console.log(form)
				var formData = new FormData(form)
    			var object = new Object();//创建一个存放数据的对象
				for (var key of formData.keys()){
					if (key == "problem_datapoint_file") {
						continue;
					}
					object[key] = formData.get(key)
				}
				var jsonData = JSON.stringify(object);
				$.ajax({
					type:"POST",
					url:"",
					data: object,
					success: function () {
						nextPage(currentPage)
						if (currentPage == totalPageNum) {
							if (problemType=="submit_answer") {
								var post_data = {};
								submission_requirement = [];
								for (var i = 1; i <= $("#n_tests-submit_answer").val(); i++) {
									submission_requirement.push({
										"name": "answer" + i,
										"type": "source code",
										"file_name": "answer" + i + ".code"
									})
								}
								post_data.submission_requirement = JSON.stringify(submission_requirement);
								var extra_config_err = 'Unknown error';
								post_data.extra_config = '{"view_content_type":"ALL","view_details_type":"ALL"}';
								$("#input-submission_requirement").val(JSON.stringify(submission_requirement));
								$("#input-extra_config").val('{"view_content_type":"ALL","view_details_type":"ALL"}');
								$("#button-submit-info").click();
							}else{
								var post_data = {};
								submission_requirement = [];
									submission_requirement.push({
										"name": "answer",
										"type": "source code",
										"file_name": "answer.code"
									})
								
								post_data.submission_requirement = JSON.stringify(submission_requirement);
								var extra_config_err = 'Unknown error';
								post_data.extra_config = '{"view_content_type":"ALL","view_details_type":"ALL"}';
								$("#input-submission_requirement").val(JSON.stringify(submission_requirement));
								$("#input-extra_config").val('{"view_content_type":"ALL","view_details_type":"ALL"}');
								$("#button-submit-info").click();
							}
							// location.reload()
						}
						return true;
					},
					error: function() {
						alert("请求失败！")
						return false;
					}
				})

			}
			$("#wizardModal").on("hidden.bs.modal", function() {
				// for (var i = 1; i<=totalPageNum; i++) {
				// 		$("#step-" + i).addClass("d-none")
				// 	}
				// $("#step-1").removeClass("d-none")
				location.reload()
			})
		</script>

	<div class="modal fade" id="UploadDataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  		<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">上传数据</h4>
        				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      				</div>
      				<div class="modal-body">
        				<form action="" method="post" enctype="multipart/form-data" role="form">
							<div class="form-group">
									<label for="exampleInputFile">上传zip文件</label>
									<input type="file" name="problem_data_file" id="problem_data_file">
									<p class="help-block">说明：请将所有数据放置于压缩包根目录内。若压缩包内仅存在文件夹而不存在文件，则会将这些一级子文件夹下的内容移动到根目录下，然后这些一级子文件夹删除；若这些子文件夹内存在同名文件，则会发生随机替换，仅保留一个副本。</p>
							</div>
							<input type="hidden" name="problem_data_file_submit" value="submit">
      				</div>
      				<div class="modal-footer">
						<button type="submit" class="btn btn-success">上传</button>
						</form>
        				<button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
      				</div>
    			</div>
  		</div>
	</div>

	<div class="modal fade" id="ProblemSettingsFileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  		<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">试题配置</h4>
        				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      				</div>
      				<div class="modal-body">
        				<form class="form-horizontal" action="" method="post" role="form">
        					<div class="form-group row">
    							<label for="use_builtin_checker" class="col-sm-5 control-label">比对函数</label>
    							<div class="col-sm-7">
								<select class="form-control" id="use_builtin_checker" name="use_builtin_checker">
  									<option value="ncmp">单行整数序列</option>
  									<option value="wcmp">单行字符串序列</option>
  									<option value="fcmp">多行数据（不忽略行末空格，但忽略文末回车）</option>
									<option value="ownchk">自定义校验器</option>
								</select>
      								<!--<input type="hidden" class="form-control" id="use_builtin_checker" name="use_builtin_checker" placeholder="比对函数">-->
    							</div>
  							</div>
  							<div class="form-group row">
    							<label for="n_tests" class="col-sm-5 control-label">n_tests</label>
    							<div class="col-sm-7">
      								<input type="text" class="form-control" id="n_tests" name="n_tests" placeholder="数据点个数">
    							</div>
  							</div>
  							<div class="form-group row">
    							<label for="n_ex_tests" class="col-sm-5 control-label">n_ex_tests</label>
    							<div class="col-sm-7">
      								<input type="text" class="form-control" id="n_ex_tests" name="n_ex_tests" placeholder="额外数据点个数">
    							</div>
  							</div>
  							<div class="form-group row">
    							<label for="n_sample_tests" class="col-sm-5 control-label">n_sample_tests</label>
    							<div class="col-sm-7">
      								<input type="text" class="form-control" id="n_sample_tests" name="n_sample_tests" placeholder="样例测试点个数">
    							</div>
  							</div>
  							<div class="form-group row">
    							<label for="input_pre" class="col-sm-5 control-label">input_pre</label>
    							<div class="col-sm-7">
      								<input type="text" class="form-control" id="input_pre" name="input_pre" placeholder="输入文件名称">
    							</div>
  							</div>
  							<div class="form-group row">
    							<label for="input_suf" class="col-sm-5 control-label">input_suf</label>
    							<div class="col-sm-7">
      								<input type="text" class="form-control" id="input_suf" name="input_suf" placeholder="输入文件后缀">
    							</div>
  							</div>
  							<div class="form-group row">
    							<label for="output_pre" class="col-sm-5 control-label">output_pre</label>
    							<div class="col-sm-7">
      								<input type="text" class="form-control" id="output_pre" name="output_pre" placeholder="输出文件名称">
    							</div>
  							</div>
  							<div class="form-group row">
    							<label for="output_suf" class="col-sm-5 control-label">output_suf</label>
    							<div class="col-sm-7">
      								<input type="text" class="form-control" id="output_suf" name="output_suf" placeholder="输出文件后缀">
    							</div>
  							</div>
  							<div class="form-group row">
    							<label for="time_limit" class="col-sm-5 control-label">time_limit</label>
    							<div class="col-sm-7">
      								<input type="text" class="form-control" id="time_limit" name="time_limit" placeholder="时间限制（不能为小数！）">
    							</div>
  							</div>
  							<div class="form-group row">
    							<label for="memory_limit" class="col-sm-5 control-label">memory_limit</label>
    							<div class="col-sm-7">
      								<input type="text" class="form-control" id="memory_limit" name="memory_limit" placeholder="内存限制">
    							</div>
  							</div>
							<input type="hidden" name="problem_settings_file_submit" value="submit">
      				</div>
      				<div class="modal-footer">
						<button type="submit" class="btn btn-success">确定</button>
						</form>
        				<button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
      				</div>
    			</div>
  		</div>
	</div>
</div>
<?php echoUOJPageFooter() ?>
